(in-package #:asdf-inferred-components-test)


;; (defun get-system ()
;;   (let* ((asdf:*central-registry*
;;            (list* (asdf:system-relative-pathname :asdf-inferred-components "test/aic-test-1/")
;;                   asdf:*central-registry*))
;;          (system (asdf:find-system :aic-test-1)))
;;     system))

(defun my-set-equal (set-1 set-2)
  (a:set-equal set-1 set-2 :test #'equal))

(p:define-test simple-one-package-test
  (let* ((asdf:*central-registry*
           (list* (asdf:system-relative-pathname :asdf-inferred-components "test/aic-test-1/")
                  asdf:*central-registry*))
         (system (asdf:find-system :aic-test-1))
         (package (asdf:find-component system '("package")))
         (a (asdf:find-component system "a"))
         (b (asdf:find-component system "b"))
         (c (asdf:find-component system "c"))
         (d (asdf:find-component system '("d" "d"))))
    (p:true package)
    (p:true a)
    (p:true b)
    (p:true c)
    (p:true d)

    (flet ((check-deps (component expected)
             (p:is my-set-equal expected (asdf:component-sideway-dependencies component))))
      (check-deps system '("x"))
      (check-deps package nil)
      (check-deps a (list package))
      (check-deps b (list package))
      (check-deps c (list package a b))
      (check-deps d (list package a)))))
