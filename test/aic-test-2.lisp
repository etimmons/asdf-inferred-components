(in-package #:asdf-inferred-components-test)


(defun get-system ()
  (let* ((asdf:*central-registry*
           (list* (asdf:system-relative-pathname :asdf-inferred-components "test/aic-test-2/")
                  asdf:*central-registry*))
         (system (asdf:find-system :aic-test-2)))
    system))

(p:define-test simple-multi-package-test
  (let* ((asdf:*central-registry*
           (list* (asdf:system-relative-pathname :asdf-inferred-components "test/aic-test-2/")
                  (asdf:system-relative-pathname :asdf-inferred-components "test/aic-test-x/")
                  asdf:*central-registry*))
         (system (asdf:find-system :aic-test-2))
         (a (asdf:find-component system "a"))
         (b (asdf:find-component system "b"))
         (c (asdf:find-component system "c"))
         (d (asdf:find-component system '("d" "d"))))
    (p:true a)
    (p:true b)
    (p:true c)
    (p:true d)

    (flet ((check-deps (component expected)
             (p:is my-set-equal expected (asdf:component-sideway-dependencies component))))
      (check-deps system '("aic-test-x"))
      (check-deps a nil)
      (check-deps b nil)
      (check-deps c (list a b))
      (check-deps d (list a)))))

(p:define-test simple-multi-package-load-test
  (let* ((asdf:*central-registry*
           (list* (asdf:system-relative-pathname :asdf-inferred-components "test/aic-test-2/")
                  (asdf:system-relative-pathname :asdf-inferred-components "test/aic-test-x/")
                  asdf:*central-registry*))
         (asdf:*system-definition-search-functions* asdf:*system-definition-search-functions*))
    (when (uiop:featurep :clpm-client)
      (uiop:symbol-call :clpm-client :deactivate-asdf-integration))
    (asdf:load-system :aic-test-2)

    (let ((a (uiop:find-package* :aic-test-2/a nil))
          (b (uiop:find-package* :aic-test-2/b nil))
          (c (uiop:find-package* :aic-test-2/c nil))
          (d (uiop:find-package* :aic-test-2/d/d nil))
          (x (uiop:find-package* :aic-test-x nil))
          (cl (uiop:find-package* :cl nil)))
      (p:true a)
      (p:true b)
      (p:true c)
      (p:true d)
      (p:true x)
      (p:true cl)

      (p:is a:set-equal (list cl) (package-use-list a))
      (p:is a:set-equal (list cl x) (package-use-list b))
      (p:is a:set-equal (list a b cl) (package-use-list c))
      (p:is a:set-equal (list cl a) (package-use-list d)))))
