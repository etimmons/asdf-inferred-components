;;;; ASDF-Inferred-Components package definition
;;;;
;;;; This file is part of the asdf-inferred-components project. See README.org
;;;; and LICENSE for more information.

(uiop:define-package #:asdf-inferred-components
    (:nicknames #:aic)
  (:use #:cl)
  (:export #:define-component
           #:define-package
           #:inferred-components-system))
