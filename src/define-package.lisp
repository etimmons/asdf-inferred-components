;;;; ASDF-Inferred-Components System Definition
;;;;
;;;; This file is part of the asdf-inferred-components project. See README.org
;;;; and LICENSE for more information.

(in-package #:asdf-inferred-components)

(defun resolve-relative-package-name (base-package-designator relative-package-designator)
  (let ((relative-name (string relative-package-designator))
        (base-name (string base-package-designator)))
    (if (uiop:string-prefix-p "." relative-name)
        (let ((split-relative-name (uiop:split-string relative-name :separator "/"))
              (name (rest (reverse (uiop:split-string base-name :separator "/")))))
          (dolist (part split-relative-name)
            (cond
              ((equal part "."))
              ((equal part "..")
               (pop name))
              (t
               (push part name))))
          (format nil "~{~A~^/~}" (reverse name)))
        relative-package-designator)))

(defun parse-define-package-form (package-name options)
  (let ((documentation nil)
        (nicknames nil)
        (use nil)
        (shadow nil)
        (shadowing-import-from nil)
        (import-from nil)
        (export nil)
        (intern nil)
        (recycle nil)
        (mix nil)
        (reexport nil)
        (unintern nil)
        (local-nicknames nil)
        (component-def (find :component-def options :key #'first))
        (no-feature (gensym)))
    (labels
        ((guard-on-feature (feature thing)
           (if (eql feature no-feature)
               `',thing
               `(when (uiop:featurep ',feature)
                  ',thing)))
         (process-package-spec (spec feature)
           (if (listp spec)
               (progn
                 (when (not (null (first spec)))
                   (push (if (eql feature no-feature)
                             (first spec)
                             `(:feature ,feature ,(first spec)))
                         (getf component-def :depends-on)))
                 (resolve-relative-package-name package-name (second spec)))
               (progn
                 (unless (or (string-equal spec :cl)
                             (string-equal spec :common-lisp))
                   (push (if (eql feature no-feature)
                             spec
                             `(:feature ,feature ,spec))
                         (getf component-def :depends-on)))
                 (resolve-relative-package-name package-name spec))))
         (process-package-list (packages feature)
           (mapcar (lambda (p) (process-package-spec p feature)) packages))
         (process-package-symbol-list (package-symbol-list feature)
           (list* (process-package-spec (first package-symbol-list) feature)
                  (rest package-symbol-list)))
         (process-symbol-package-list (symbol-package-list feature)
           (list* (first symbol-package-list)
                  (mapcar (lambda (p) (process-package-spec p feature)) (rest symbol-package-list))))
         (parse-option (option &optional (feature no-feature))
           (destructuring-bind (kw &rest args) option
             (ecase kw
               (:nicknames
                (push (guard-on-feature feature `(quote ,args)) nicknames))
               (:documentation
                (cond
                  (documentation (error "Can't define documentation twice"))
                  ((or (not (stringp (first args))) (rest args)) (error "Bad documentation"))
                  (t (setf documentation (first args)))))
               (:use
                (push (guard-on-feature feature (process-package-list args feature))
                      use))
               (:shadow
                (push (guard-on-feature feature args)
                      shadow))
               (:shadowing-import-from
                (push (guard-on-feature feature (process-package-symbol-list args feature))
                      shadowing-import-from))
               (:import-from
                (push (guard-on-feature feature (process-package-symbol-list args feature))
                      import-from))
               (:export
                (push (guard-on-feature feature args)
                      export))
               (:intern
                (push (guard-on-feature feature args)
                      intern))
               (:recycle
                (push (guard-on-feature feature (process-package-list args feature))
                      recycle))
               (:mix
                (push (guard-on-feature feature (process-package-list args feature))
                      mix))
               (:reexport
                (push (guard-on-feature feature (process-package-list args feature))
                      reexport))
               (:use-reexport
                (push (guard-on-feature feature (process-package-list args feature))
                      use)
                (push (guard-on-feature feature (process-package-list args feature))
                      reexport))
               (:mix-reexport
                (push (guard-on-feature feature (process-package-list args feature))
                      mix)
                (push (guard-on-feature feature (process-package-list args feature))
                      reexport))
               (:unintern
                (push (guard-on-feature feature args)
                      unintern))
               (:local-nicknames
                (if (uiop:featurep :package-local-nicknames)
                    (push (guard-on-feature feature (mapcar (lambda (x) (process-symbol-package-list x feature))
                                                            args))
                          local-nicknames)
                    (error ":LOCAL-NICKNAMES option is not supported on this implementation.")))
               (:feature
                (mapc (lambda (o) (parse-option o (first args))) (rest args)))))))
      (mapc #'parse-option (remove component-def options)))
    (values
     (list :nicknames `(append ,@(reverse nicknames))
           :documentation documentation
           :use (if use `(append ,@(reverse use)) '(quote (:common-lisp)))
           :shadow `(append ,@(reverse shadow))
           :shadowing-import-from `(remove nil (list ,@(reverse shadowing-import-from)))
           :import-from `(remove nil (list ,@(reverse import-from)))
           :export `(append ,@(reverse export))
           :intern `(append ,@(reverse intern))
           :recycle (if recycle `(append ,@(reverse recycle))
                        `(cons ',package-name (append ,@(reverse nicknames))))
           :mix `(append ,@(reverse mix))
           :reexport `(append ,@(reverse reexport))
           :unintern `(append ,@(reverse unintern))
           :local-nicknames `(append ,@(reverse local-nicknames)))
     component-def)))

(defmacro define-package (package-name &rest options)
  "Roughly equivalent to a AIC:DEFINE-COMPONENT form and a UIOP:DEFINE-PACKAGE form.

The option (:COMPONENT-DEF &REST PLIST) is used to define the options for
creating the component. Every pair in PLIST is treated the same as if it were
provided to AIC:DEFINE-COMPONENT. There may be only one :COMPONENT-DEF option.

There may be any number of options of the form (:FEATURE FEATURE-EXPRESSION &REST PACKAGE-OPTIONS).
Every option in PACKAGE-OPTIONS is treated as described below, but its effects
only happen if FEATURE-EXPRESSION evaluates to true (see UIOP:FEATUREP).

The remaining options are the package defining options. These options behave
the same as if they were passed to UIOP:DEFINE-PACKAGE. However, anywhere a
package name is expected this macro also accepts a relative package name or a
system name/package name pair.

For example, if PACKAGE-NAME is A/B/C, the parent system is named A, and
OPTIONS contains a form (:USE #:../D/E), the package will use the package A/D/E
and depend on the component (\"D\" \"E\") in system A.

If OPTIONS contains a form (:USE (#:CL-Z #:Z)), the package will use the
package Z and depend on the system CL-Z. If the first item in system/package
pair is NIL, no dependencies are extracted."
  (let ((ensure-form
          `(prog1
               (funcall 'uiop:ensure-package ',package-name
                        ,@(parse-define-package-form package-name options))
             #+sbcl (setf (sb-impl::package-source-location (find-package ',package-name))
                          (sb-c:source-location)))))
    `(progn
       #+(or clasp ecl gcl mkcl) (defpackage ,package-name (:use))
       (eval-when (:compile-toplevel :load-toplevel :execute)
         ,ensure-form))))

(defmethod component-defining-list-to-plist (parent wrapper (operator (eql 'define-package)) args)
  (destructuring-bind (&rest args &key depends-on &allow-other-keys)
      (nth-value 1 (parse-define-package-form (first args) (rest args)))
    (let ((parent-path (component-path parent)))
      (list* :depends-on (mapcar (lambda (x) (parse-dependency-specifier parent-path x)) depends-on)
             (uiop:remove-plist-key :depends-on args)))))
