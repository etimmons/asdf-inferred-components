;;;; file wrappers
;;;;
;;;; This file is part of the asdf-inferred-components project. See README.org
;;;; and LICENSE for more information.

(in-package #:asdf-inferred-components)

(defparameter *file-type-wrapper-map*
  '(("lisp" . cl-file-wrapper)
    ("cl" . cl-file-wrapper)
    ("lsp" . cl-file-wrapper)
    ("asd" . ignore-file-wrapper)
    (t . unknown-file-wrapper)))

(defclass file-wrapper ()
  ((namestring
    :initarg :namestring
    :reader file-wrapper-namestring))
  (:documentation
   "Wraps a file namestring in an object based on an inferred type. This allows
us to dispatch on inferred file types more easily using generic functions."))

(defun file-wrapper-type (wrapper)
  (pathname-type (file-wrapper-namestring wrapper)))

(defun file-wrapper-name (wrapper)
  (pathname-name (file-wrapper-namestring wrapper)))

(defclass ignore-file-wrapper (file-wrapper)
  ()
  (:documentation
   "A file that should be ignored and not registered as a component."))

(defclass cl-file-wrapper (file-wrapper)
  ()
  (:documentation
   "A Common Lisp source file."))

(defclass unknown-file-wrapper (file-wrapper)
  ()
  (:documentation
   "An unknown file."))
