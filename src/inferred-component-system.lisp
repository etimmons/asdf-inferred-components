;;;; system class and ASDF integration
;;;;
;;;; This file is part of the asdf-inferred-components project. See README.org
;;;; and LICENSE for more information.

(in-package #:asdf-inferred-components)

(defvar *hoisted-deps*)

(defclass inferred-components-parent-component (asdf:parent-component)
  ()
  (:documentation
   "Base class for a component that infers its children."))

(defclass inferred-components-system (inferred-components-parent-component asdf:system)
  ((hoisted-deps
    :initform nil
    :accessor inferred-components-system-hoisted-deps)
   (cached-complete-deps
    :accessor inferred-components-system-cached-complete-deps))
  (:documentation
   "A system class that automatically includes all files underneath the
system's pathname as components."))

(defmethod asdf:additional-input-files ((op asdf:define-op) (system inferred-components-system))
  "The actual system definition itself depends on all the files that could
become inferred components."
  (append (call-next-method)
          (remove (asdf:system-source-file system)
                  (remove-if-not #'uiop:file-pathname-p
                                 (uiop:directory* (merge-pathnames uiop:*wild-path* (asdf:component-pathname system))))
                  :test #'uiop:pathname-equal)))

(defmethod (setf asdf:component-sideway-dependencies) :after (value (system inferred-components-system))
  "Clear the cache of out complete set of sideway deps."
  (slot-makunbound system 'cached-complete-deps))

(defmethod (setf inferred-components-system-hoisted-deps) :after (value (system inferred-components-system))
  "Clear the cache of out complete set of sideway deps."
  (slot-makunbound system 'cached-complete-deps))

(defmethod asdf:component-sideway-dependencies ((system inferred-components-system))
  "ASDF sets the SIDEWAY-DEPENDENCIES slot directly as part of
PARSE-COMPONENT-FORM and there's no easy way to hijack that behavior. So our
complete set of dependencies is the union of explicit deps and deps hoisted
from our children. Additionally, cache this so we don't have to continuously
UNION things."
  (if (slot-boundp system 'cached-complete-deps)
      (inferred-components-system-cached-complete-deps system)
      (setf (inferred-components-system-cached-complete-deps system)
            (union (call-next-method) (inferred-components-system-hoisted-deps system)
                   :test #'equal))))

(defclass inferred-components-module (inferred-components-parent-component asdf:module)
  ()
  (:documentation
   "A module (representing a directory) that infers children."))

(defgeneric inferred-components-module-class (system)
  (:documentation
   "Potential extension point. Decide which class to use when instantiating a
module."))

(defmethod inferred-components-module-class ((system inferred-components-parent-component))
  'inferred-components-module)

(defmethod asdf::class-for-type ((parent inferred-components-system) type)
  "Tell ASDF which module class to use."
  (if (eql type :module)
      (inferred-components-module-class parent)
      (call-next-method)))

(defmethod asdf::class-for-type ((parent inferred-components-module) type)
  "Tell ASDF which module class to use."
  (if (eql type :module)
      (class-of parent)
      (call-next-method)))

(defgeneric pathname-type-to-wrapper-class (system type)
  (:documentation
   "Potential extension point. Return the FILE-WRAPPER class to use in order to
represent the given file TYPE for SYSTEM."))

(defmethod pathname-type-to-wrapper-class ((system inferred-components-parent-component) type)
  "The default falls back to the *FILE-TYPE-WRAPPER-MAP* alist."
  (uiop:if-let ((cell (assoc type *file-type-wrapper-map* :test #'equal)))
    (cdr cell)
    (cdr (assoc t *file-type-wrapper-map*))))

(defgeneric pathname-to-wrapper (system pn)
  (:documentation
   "Potential extension point. Return the FILE-WRAPPER for the file at PN."))

(defmethod pathname-to-wrapper ((system inferred-components-parent-component) pn)
  "Infer the class of the file wrapper by file type."
  (make-instance (pathname-type-to-wrapper-class system (pathname-type pn))
                 :namestring pn))

(defgeneric file-wrapper-extract-options (parent wrapper)
  (:documentation
   "Potential extension point. Given a WRAPPER that is an inferred child of
PARENT, return a plist of options to use when creating the component."))

(defmethod file-wrapper-extract-options (parent (wrapper cl-file-wrapper))
  (let ((defining-form (uiop:safe-read-file-form (merge-pathnames (file-wrapper-namestring wrapper)
                                                                  (asdf:component-pathname parent)))))
    (if (component-defining-form-p parent wrapper defining-form)
        (values (component-defining-form-to-plist parent wrapper defining-form) t)
        (values nil nil))))

(defun hoist-dep-p (dep)
  "Returns non-NIL if DEP should be hoisted to the parent system."
  (etypecase dep
    (string
     t)
    (list
     (etypecase (first dep)
       (asdf:component nil)
       ((eql :version)
        (hoist-dep-p (second dep)))
       ((eql :feature)
        (hoist-dep-p (third dep)))))))

(defun split-deps (deps)
  "Returns two VALUES. The first is a set of local dependencies, the second is
a set of dependencies to be hoisted to the parent system."
  (let* ((hoisted-deps (remove-if-not #'hoist-dep-p deps))
         (not-hoisted-deps (set-difference deps hoisted-deps)))
    (values not-hoisted-deps hoisted-deps)))

(defgeneric ensure-inferred-component (parent wrapper)
  (:documentation
   "Potential extension point. Ensure the inferred component exists and it has
been created with the correct options."))

(defmethod ensure-inferred-component (parent
                                      (wrapper unknown-file-wrapper))
  (uiop:if-let ((component (asdf:find-component parent (file-wrapper-namestring wrapper))))
    component
    (asdf::parse-component-form parent `(:static-file ,(file-wrapper-namestring wrapper)))))

(defmethod ensure-inferred-component (parent (wrapper ignore-file-wrapper))
  nil)

(defmethod ensure-inferred-component (parent wrapper)
  (multiple-value-bind (options options-exist-p)
      (file-wrapper-extract-options parent wrapper)
    (let ((component (asdf:find-component parent (file-wrapper-name wrapper))))
      (if (or options-exist-p (null component))
          (let ((deps (getf options :depends-on)))
            (multiple-value-bind (local-deps hoisted-deps)
                (split-deps deps)
              (setf *hoisted-deps* (append hoisted-deps *hoisted-deps*))
              (asdf::parse-component-form parent `(,(or (getf options :component-type)
                                                        :file)
                                                   ,(file-wrapper-name wrapper)
                                                   :type ,(file-wrapper-type wrapper)
                                                   :depends-on ,local-deps
                                                   ,@(uiop:remove-plist-keys '(:component-type
                                                                               :depends-on)
                                                                             options)))))
          component))))

(defmethod asdf::compute-component-children ((parent inferred-components-parent-component)
                                             components serial-p)
  "Hijack the normal ASDF computation of children. CALL-NEXT-METHOD to process
the explicit children, then find and create the implicit components."
  (let ((explicit-children (call-next-method))
        (implicit-children nil)
        (component-pathname (asdf:component-pathname parent)))

    ;; ABCL, ECL, and {SBCL, CCL} appear to have all different ways of parsing
    ;; wildcards in DIRECTORY. On SBCL and CCL, *WILD-FILE-FOR-DIRECTORY* gives
    ;; both files and sub directories. On ECL, *WILD-FILE-FOR-DIRECTORY* gives
    ;; only files. On ABCL, *WILD-DIRECTORY* gives files in sub-directories.
    (dolist (item (remove-duplicates
                   (append (uiop:directory* (merge-pathnames uiop:*wild-file-for-directory*
                                                             component-pathname))
                           (unless (uiop:featurep :abcl)
                             (uiop:directory* (merge-pathnames uiop:*wild-directory*
                                                               component-pathname))))
                   :test #'uiop:pathname-equal))

      (if (uiop:file-pathname-p item)
          (let* ((file-name (file-namestring item))
                 (wrapper (pathname-to-wrapper parent file-name))
                 (c (ensure-inferred-component parent wrapper)))
            (when c
              (push c implicit-children)))
          (unless (some (lambda (x) (uiop:pathname-equal (asdf:component-pathname x) item))
                        explicit-children)
            ;; Only recurse if the directory was *not* included in the explicit
            ;; children.
            (push (asdf::parse-component-form parent `(:module ,(first (last (pathname-directory item)))))
                  implicit-children))))
    (union explicit-children implicit-children :test #'equal)))

(defgeneric fixup-inferred-deps (component)
  (:documentation
   "The dependencies stored in the componet definitions cannot be arbitrary
paths recognized by FIND-COMPONENT. This takes any list based dependencies and
maps any that are not :FEATURE or :VERSION deps to raw component references."))

(defun fixup-inferred-dep (component dep)
  (if (listp dep)
      (etypecase (first dep)
        (asdf:component
         (asdf:find-component component dep))
        ((eql :version)
         (list* :version (fixup-inferred-dep component (second dep)) (rest (rest dep))))
        ((eql :feature)
         (list :feature (second dep) (fixup-inferred-dep component (third dep)))))
      dep))

(defmethod fixup-inferred-deps ((component asdf:component))
  (setf (asdf:component-sideway-dependencies component)
        (mapcar (lambda (d) (fixup-inferred-dep (asdf:component-parent component) d))
                (asdf:component-sideway-dependencies component))))

(defmethod fixup-inferred-deps ((component asdf:parent-component))
  (mapc #'fixup-inferred-deps (asdf:component-children component)))

(defmethod asdf::compute-component-children :around ((system inferred-components-system)
                                                     components serial-p)
  "Establish a binding for *HOISTED-DEPS*, CALL-NEXT-METHOD, and then process
the hoisted deps/otherwise fixup inferred dependencies."
  (let* ((*hoisted-deps* nil)
         (children (call-next-method)))
    (setf (inferred-components-system-hoisted-deps system)
          *hoisted-deps*)
    (setf (asdf:component-children system) children)
    (asdf::compute-children-by-name system)
    (mapc #'fixup-inferred-deps children)
    children))

(defgeneric component-defining-form-to-plist (parent wrapper form)
  (:documentation
   "Possible extension point. Given a component-defining form, return a plist
of component options."))

(defmethod component-defining-form-to-plist (parent wrapper form)
  (component-defining-list-to-plist parent wrapper (first form) (rest form)))

(defgeneric component-defining-list-to-plist (parent wrapper operator args)
  (:documentation
   "Possible extension point. Given a component-defining form OPERATOR and
ARGS, return a plist of component options."))

(defgeneric component-path (component))

(defmethod component-path ((component asdf:system))
  (list component))

(defmethod component-path ((component asdf:component))
  (reverse (list* (asdf:component-name component) (component-path (asdf:component-parent component)))))

(defgeneric component-defining-form-p (parent wrapper form))

(defmethod component-defining-form-p (parent wrapper form)
  (and (listp form)
       (or (eql (first form) 'define-component)
           (eql (first form) 'define-package))))
