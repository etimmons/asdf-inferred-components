;;;; ASDF-Inferred-Components System Definition
;;;;
;;;; This file is part of the asdf-inferred-components project. See README.org
;;;; and LICENSE for more information.

(in-package #:asdf-inferred-components)

(defmacro define-component (&rest args &key depends-on component-type &allow-other-keys)
  "Define a new file component for a system. This (currently) expands
to (PROGN) (i.e., it does nothing when the file is compiled or loaded).

If COMPONENT-TYPE is provided, it must be something usable as the first form in
the component definition. If not provided, it defaults to :FILE.

If DEPENDS-ON is provided, it must be a list of dependency definitions. The
provided list is processed before being handed off to ASDF's built in
machinery.

If the component name of any dependency starts with a #\. character, it is
turned into an intrasystem dependency. To do so, the component name is parsed
as a pathname relative to the file containing the defining form. Otherwise, the
dependency is assumed to refer to a system.

Every other option must be a keyword argument valid for explicitly defining a
component."
  (declare (ignore args depends-on component-type))
  `(progn))

(defun resolve-dependency-component-name (parent-path id)
  (if (uiop:string-prefix-p "." id)
      (let ((relative-path (uiop:parse-unix-namestring id :want-relative t))
            (path (reverse parent-path)))
        (dolist (dir (rest (pathname-directory relative-path)))
          (if (or (eql :back dir)
                  (eql :up dir))
              (pop path)
              (push dir path)))
        (push (pathname-name relative-path) path)
        (reverse path))
      id))

(defun parse-dependency-specifier (parent-path spec)
  (etypecase spec
    (string
     (resolve-dependency-component-name parent-path spec))
    (symbol
     (resolve-dependency-component-name parent-path (asdf:coerce-name spec)))
    (cons
     (ecase (first spec)
       (:version
        (list* :version (parse-dependency-specifier parent-path (second spec))
               (rest (rest spec))))
       (:feature
        (list :feature (second spec) (parse-dependency-specifier parent-path (third spec))))))))

(defmethod component-defining-list-to-plist (parent wrapper (operator (eql 'define-component)) args)
  (destructuring-bind (&rest args &key depends-on &allow-other-keys) args
    (let ((parent-path (component-path parent)))
      (list* :depends-on (mapcar (lambda (x) (parse-dependency-specifier parent-path x)) depends-on)
             (uiop:remove-plist-key :depends-on args)))))
