;;;; ASDF-Inferred-Components System Definition
;;;;
;;;; This file is part of the asdf-inferred-components project. See README.org
;;;; and LICENSE for more information.

(defsystem #:asdf-inferred-components
  :version (:read-file-form "version.lisp-expr")
  :license "MIT"
  :pathname "src"
  :in-order-to ((test-op (load-op "asdf-inferred-components/test")))
  :perform (test-op (o c)
                    (unless (eql :passed (uiop:symbol-call
                                          :parachute :status
                                          (uiop:symbol-call :parachute :test :asdf-inferred-components-test)))
                      (error "Tests failed")))
  :components ((:file "package")
               (:file "file-wrappers" :depends-on ("package"))
               (:file "inferred-component-system" :depends-on ("package" "file-wrappers"))
               (:file "define-component" :depends-on ("package" "inferred-component-system"))
               (:file "define-package" :depends-on ("package" "inferred-component-system" "define-component")))
  :depends-on ((:version #:asdf "3.3.5")))

(defsystem #:asdf-inferred-components/test
  :version (:read-file-form "version.lisp-expr")
  :license "MIT"
  :pathname "test"
  :components ((:file "package")
               (:file "aic-test-1")
               (:file "aic-test-2"))
  :depends-on (#:alexandria #:parachute))
